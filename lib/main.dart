import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<String> _products = ['Food Tester'];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          brightness: Brightness.light, primarySwatch: Colors.deepOrange),
      title: 'Hello Word',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Assena Damas'),
        ),
        body: Column(children: <Widget>[
          Container(
            margin: EdgeInsets.all(10.0),
            child: RaisedButton(
              child: Text('Add Product'),
              onPressed: () {
                setState(() {
                  _products.add('Food Vaganza');
                  print(_products);
                });
              },
            ),
          ),
          Column(
            children: _products
                .map(
                  (el) => Card(
                        child: Column(
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.all(10.0),
                                child: Image.asset('assets/food.jpg')),
                            Container(
                                margin: EdgeInsets.all(10.0), child: Text(el)),
                          ],
                        ),
                      ),
                )
                .toList(),
          ),
        ]),
      ),
    );
  }
}
